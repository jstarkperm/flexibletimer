package com.flexibletimer;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.TextView;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private TextView textViewTimeToLoop;
    private Chronometer chronometer;
    private ImageButton buttonStart;

    private int counter;
    private int preCounter;
    private int preCounterForTicks;
    private boolean mStarted = false;


    private ArrayList<String> timerGoals = new ArrayList<>();
    {
        timerGoals.add("15");
        timerGoals.add("20");
        timerGoals.add("30");
        preCounter = 3;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.chronometer = findViewById(R.id.chronometer);

        this.textViewTimeToLoop = findViewById(R.id.textView_TimeToLoop);

        counter = Integer.parseInt(this.textViewTimeToLoop.getText().toString());

        this.buttonStart = findViewById(R.id.button_Start);

        this.chronometer.setText(counter + "");

        this.buttonStart.setOnClickListener(view -> doStart());

        this.chronometer.setOnChronometerTickListener(chronometer -> onChronometerTickHandler());

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    private void doStart() {
        if (!mStarted) {
            preCounterForTicks = preCounter + 1;
            counter = Integer.parseInt(this.textViewTimeToLoop.getText().toString());
            this.chronometer.start();
            this.buttonStart.setImageResource(R.drawable.stop);
            mStarted = true;
        } else {
            this.chronometer.stop();
            this.buttonStart.setImageResource(R.drawable.start);
            mStarted = false;
            this.chronometer.setTextColor(Color.rgb(0, 0, 0));
            counter = Integer.parseInt(this.textViewTimeToLoop.getText().toString());
            this.chronometer.setText(counter + "");
        }
    }

    private void onChronometerTickHandler() {
        preCounterForTicks--;
        if (preCounterForTicks > 0) {
            this.chronometer.setTextColor(Color.rgb(200, 0, 0));
            this.chronometer.setText(preCounterForTicks + "");
        }
        else {
            this.chronometer.setTextColor(Color.rgb(0, 0, 0));
            this.chronometer.setText(counter + "");
            counter--;
        }
        if (counter < 0) {
            preCounterForTicks = preCounter + 1;
            counter = Integer.parseInt(this.textViewTimeToLoop.getText().toString());
        }
    }

    public void onClickTextView(View view) {
        for (int i = 0; i < timerGoals.size(); i++) {
            if (timerGoals.get(i).equals(this.textViewTimeToLoop.getText().toString())) {
                if (i == timerGoals.size() - 1) {
                    this.textViewTimeToLoop.setText(timerGoals.get(0));
                    break;
                } else this.textViewTimeToLoop.setText(timerGoals.get(i + 1));
                break;
            }
        }
    }

}